# SZSTool

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

A tool (inspired by Sarc-Tool) that can unpack / repack .szs and .sarc Files

## Usage

### Python
Packing: `py -3 szstool.py -en [option...] file/folder`

Unpacking: `py -3 szstool.py file/folder`

### Executable
Packing: `szstool.exe -en [option...] file/folder`

Unpacking: `szstool.exe file/folder`

## Requirements for SZSTool
Python 3.x (or the .exe version)

szsLib (Get it by using `py -3 pip install szsLib` )

yazLib (Get it by using `py -3 pip install yazLib` )